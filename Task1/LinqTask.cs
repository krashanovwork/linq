﻿using System;
using System.Collections.Generic;
using Task1.DoNotChange;
using System.Linq;

namespace Task1
{
    public static class LinqTask
    {
        public static IEnumerable<Customer> Linq1(IEnumerable<Customer> customers, decimal limit) =>
            customers.Where(x => x.Orders.Sum(y => y.Total) > limit);

        public static IEnumerable<(Customer customer, IEnumerable<Supplier> suppliers)> Linq2(
            IEnumerable<Customer> customers,
            IEnumerable<Supplier> suppliers
        ) =>
            customers.Select(customer => (
                customer,
                suppliers.Where(supplier => supplier.Country == customer.Country &&
                                            supplier.City == customer.City)));

        public static IEnumerable<(Customer customer, IEnumerable<Supplier> suppliers)> Linq2UsingGroup(
            IEnumerable<Customer> customers,
            IEnumerable<Supplier> suppliers
        ) =>
            customers.GroupJoin(suppliers,
                c => (c.Country, c.City),
                s => (s.Country, s.City),
                (customer, supplier) => (customer, supplier));

        public static IEnumerable<Customer> Linq3(IEnumerable<Customer> customers, decimal limit) =>
            customers.Where(x => x.Orders.Any(y => y.Total > limit));

        public static IEnumerable<(Customer customer, DateTime dateOfEntry)> Linq4(
            IEnumerable<Customer> customers
        ) =>
            customers.Where(c => c.Orders.Any())
                     .Select(c => (c, c.Orders.Min(d => d.OrderDate)));

        public static IEnumerable<(Customer customer, DateTime dateOfEntry)> Linq5(
            IEnumerable<Customer> customers
        ) =>
            customers.Where(c => c.Orders.Any())
                     .Select(c => (c, c.Orders.Min(d => d.OrderDate)))
                     .OrderBy(y => y.Item2.Year)
                     .ThenBy(m => m.Item2.Month)
                     .ThenByDescending(n => n.c.CustomerID);

        public static IEnumerable<Customer> Linq6(IEnumerable<Customer> customers)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<Linq7CategoryGroup> Linq7(IEnumerable<Product> products)
        {
            /* example of Linq7result

             category - Beverages
	            UnitsInStock - 39
		            price - 18.0000
		            price - 19.0000
	            UnitsInStock - 17
		            price - 18.0000
		            price - 19.0000
             */

            throw new NotImplementedException();
        }

        public static IEnumerable<(decimal category, IEnumerable<Product> products)> Linq8(
            IEnumerable<Product> products,
            decimal cheap,
            decimal middle,
            decimal expensive
        )
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<(string city, int averageIncome, int averageIntensity)> Linq9(
            IEnumerable<Customer> customers
        )
        {
            throw new NotImplementedException();
        }

        public static string Linq10(IEnumerable<Supplier> suppliers) =>
            suppliers
                .Select(x => x.Country)
                .Distinct()
                .OrderBy(y => y.Length)
                .ThenBy(z => z)
                .Aggregate((s1, s2) => s1 + s2);
    }
}